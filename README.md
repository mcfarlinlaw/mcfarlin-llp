McFarlin LLP provides legal services to small to mid-sized businesses & property owners in California. Experienced in business litigation & real estate litigation. McFarlin LLP has a renowned bankruptcy and foreclosure defense practice, including bankruptcy litigation.

Address: 4 Park Plaza, Suite 1025, Irvine, CA 92614, USA

Phone: 949-544-2640
